﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;


/**
 * Este plugin es utilizado para completar distintos lookups de registros creados mediante un web service de un tercer
 * proveedor de Scanco no puede enviar los guids por el web service (si yo se)
 * De momento se va usar para las entidades Lead y Caso, y debe registrarse como precreate.
 */
namespace ScancoLookUpsWebService
{
	public class PreWebServiceCreate : IPlugin
	{
		public void Execute(IServiceProvider serviceProvider)
		{
			IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
			ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
			IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
			IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);
			ScancoServiceContext crmContext = new ScancoServiceContext(service);

			Entity entity;
			if (!context.InputParameters.Contains("Target") || (context.InputParameters["Target"] is Entity) == false) return;
			entity = (Entity)context.InputParameters["Target"];

			try
			{
				
				if (entity.LogicalName == Incident.EntityLogicalName)
				{
					Incident caso = entity.ToEntity<Incident>();
					if (caso.Contains("caseorigincode") && caso.CaseOriginCode.Value == 3) // 3 es web
						processIncident(caso, crmContext);
				}

				if (entity.LogicalName == Lead.EntityLogicalName)
				{
					Lead lead = entity.ToEntity<Lead>();
					if (lead.Contains("leadsourcecode") && lead.LeadSourceCode.Value == 8) //8 es web
						processLead(lead, crmContext);
				}
				return;
			}
			catch(Exception e)
			{
				tracingService.Trace(string.Format("PreWebServiceCreate: {0}, error: {1}", entity.LogicalName, e.Message));
			}
		
		}

		private void processLead(Lead lead, ScancoServiceContext crmContext)
		{
			if (lead.Contains("new_spais"))
				lead.new_paisid1 = GetPais(lead.new_sPais, crmContext);
		}

		private void processIncident(Incident caso, ScancoServiceContext crmContext)
		{

			if (caso.Contains("new_spais"))
				caso.new_paisid = GetPais(caso.new_sPais, crmContext);

			if (caso.Contains("new_smarca1"))
				caso.new_marcaid = GetMarca(caso.new_sMarca1, crmContext);

			if (caso.Contains("new_smarca2"))
				caso.new_Marca2id = GetMarca(caso.new_sMarca2, crmContext);

			if (caso.Contains("new_smarca3"))
				caso.new_Marca3id = GetMarca(caso.new_sMarca3, crmContext);

			if (caso.Contains("new_smarca4"))
				caso.new_Marca4id = GetMarca(caso.new_sMarca4, crmContext);

			if (caso.Contains("new_sinstrumento1"))
				caso.new_Instrumento1id = GetInstrument(caso.new_sInstrumento1, crmContext);

			if(caso.Contains("new_sinstrumento2"))
				caso.new_Instrumento2id = GetInstrument(caso.new_sInstrumento2, crmContext);

			if (caso.Contains("new_sinstrumento3"))
				caso.new_Instrumento3id = GetInstrument(caso.new_sInstrumento3, crmContext);

			if (caso.Contains("new_sinstrumento4"))
				caso.new_Instrumento4id = GetInstrument(caso.new_sInstrumento4, crmContext);
		}

		private EntityReference GetInstrument(string nombre, ScancoServiceContext crmContext)
		{
			EntityReference producto = null;

			var productos = (from p in crmContext.new_productosSet
							 where p.new_name == nombre
						  select new EntityReference
						  {
							  Id = p.Id,
							  LogicalName = new_productos.EntityLogicalName
						  }).ToList();

			if (productos.Count > 0)
				producto = productos.ElementAt(0);

			return producto;
		}

		private EntityReference GetMarca(string nombre, ScancoServiceContext crmContext)
		{
			EntityReference marca = null;

			var marcas = (from p in crmContext.new_brandSet
						  where p.new_name == nombre
						  select new EntityReference
						  {
							  Id = p.Id,
							  LogicalName = new_brand.EntityLogicalName
						  }).ToList();

			if (marcas.Count > 0)
				marca = marcas.ElementAt(0);

			return marca;
		}

		private EntityReference GetPais(string nombre, ScancoServiceContext crmContext)
		{
			EntityReference pais = null;

			var paises = (from p in crmContext.new_paisSet
						  where p.new_name == nombre
						  select new EntityReference
						  {
							  Id = p.Id,
							  LogicalName = new_pais.EntityLogicalName
						  }).ToList();

			if (paises.Count > 0)
				pais = paises.ElementAt(0);

			return pais;
		}
	}
}
